﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TalentManager.Domain;
using TalentManager.Data;
using Rhino.Mocks;
using AutoMapper;
using TalentManager.Web.Models;
using TalentManager.Web.Controllers;
using System.Net.Http;
using System.Net;
using System.Web.Http;

namespace EmployeesControllerTest
{
    [TestClass]
    public class EmployeesControllerTest
    {
        [TestMethod]
        public void MustReturnEmployeeForGetUsingAValidId()
        {
            //Arrange 
            int id = 12345;
            var employee = new Employee() { 
                Id = id, FirstName="John",LastName="Human"
            };
            IRepository<Employee> repository = MockRepository.GenerateMock<IRepository<Employee>>();
            repository.Stub(x => x.Find(id)).Return(employee);
            IUnitOfWork uow = MockRepository.GenerateMock<IUnitOfWork>();
            Mapper.CreateMap<Employee, EmployeeDto>();
            var controller = new EmployeesController(uow, repository, Mapper.Engine);
            controller.EnsureNotNull();
            //Act
            HttpResponseMessage response = controller.Get(id);
            //Assert
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Content);
            Assert.IsInstanceOfType(response.Content, typeof(ObjectContent<EmployeeDto>));
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            var content = response.Content as ObjectContent<EmployeeDto>;
            var result = content.Value as EmployeeDto;
            Assert.AreEqual(result.Id, employee.Id);
            Assert.AreEqual(result.FirstName, employee.FirstName);
            Assert.AreEqual(result.LastName, employee.LastName);

        }
        [TestMethod]
        public void MustReturn404WhenForGetUsingAnInvalidId() {
            int invalidId = 123456;
            IRepository<Employee> repository = MockRepository.GenerateMock<IRepository<Employee>>();
            repository.Stub(x => x.Find(invalidId)).Return(null);
            IUnitOfWork uow = MockRepository.GenerateMock<IUnitOfWork>();
            Mapper.CreateMap<Employee, EmployeeDto>();
            var controller = new EmployeesController(uow, repository, Mapper.Engine);

            controller.EnsureNotNull();

            HttpResponseMessage response = null;
            try
            {
                response = controller.Get(invalidId);
                Assert.Fail();
            }
            catch (HttpResponseException ex)
            {

                Assert.AreEqual(HttpStatusCode.NotFound, ex.Response.StatusCode);
            }
        }
        [TestMethod]
        public void MustReturn201AndLinkForPost() { 
            //Arrange
            int id = 12345;
            var employeeDto = new EmployeeDto() { Id = id, FirstName = "John", LastName = "Human" };
            string requestUri = "http://localhost:8086/api/employees";
            Uri uriForNewEmployee = new Uri("http://localhost:8086/api/employees/12345");
            IRepository<Employee> repository = MockRepository.GenerateMock<IRepository<Employee>>();
            repository.Expect(x => x.Insert(null)).IgnoreArguments().Repeat.Once();
            IUnitOfWork uow = MockRepository.GenerateMock<IUnitOfWork>();
            uow.Expect(x => x.Save()).Return(1).Repeat.Once();
            Mapper.CreateMap<EmployeeDto, Employee>();
            var controller = new EmployeesController(uow, repository, Mapper.Engine);
            controller.SetRequest("employees", HttpMethod.Post, requestUri);

            
            //Act
            var response = controller.Post(employeeDto);
            //Assert
            repository.VerifyAllExpectations();
            uow.VerifyAllExpectations();
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
            Assert.AreEqual(uriForNewEmployee, response.Headers.Location);
        }
    }
}
