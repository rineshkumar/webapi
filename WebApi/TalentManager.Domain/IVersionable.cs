﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TalentManager.Domain
{
    public interface IVersionable
    {
        byte[] RowVersion { get; set; }
    }
}
