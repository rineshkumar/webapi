﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http.Filters;
using TalentManager.Domain;
using TalentManager.Web.Models;

namespace TalentManager.Web.App_Start
{
    public class OptimisticLockAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            var request = actionExecutedContext.Request;

            if (request.Method == HttpMethod.Get)
            {
                object content = (actionExecutedContext.Response.Content as ObjectContent).Value;
                if (content is IVersionable)
                {
                    var rowVersion = ((IVersionable)content).RowVersion;
                    var etag = new EntityTagHeaderValue("\""+Convert.ToBase64String(rowVersion)+"\"");
                    actionExecutedContext.Response.Headers.ETag = etag;
                }
            }

        }
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext context)
        {
            var request = context.Request;
            if (request.Method == HttpMethod.Put)
            {
                EntityTagHeaderValue etagFromClient = request.Headers.IfMatch.FirstOrDefault();
                if (etagFromClient != null)
                {
                    var rowVersion = Convert.FromBase64String(
                    etagFromClient.Tag.Replace("\"", String.Empty));
                    foreach (var x in context.ActionArguments.Values.Where(v => v is IVersionable))
                    {
                        ((IVersionable)x).RowVersion = rowVersion;
                    }
                }
            }
        }
    }
}