﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;

namespace TalentManager.Web.App_Start
{
    public class MyNotSoImportantHandler : DelegatingHandler
    {
        private const string REQUEST_HEADER = "X-Name2";
        private const string RESPONSE_HEADER = "X-Message2";
        private const string NAME = "Potter";
        protected override async System.Threading.Tasks.Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            string name = string.Empty;
            if (request.Headers.Contains(REQUEST_HEADER)) {
                name = request.Headers.GetValues(REQUEST_HEADER).First();
                if (NAME.Equals(name, StringComparison.OrdinalIgnoreCase))
                {
                    return request.CreateResponse(HttpStatusCode.Forbidden);
                }

            }
            var response = await base.SendAsync(request, cancellationToken);
            if (response.StatusCode == HttpStatusCode.OK && name != null) {
                response.Headers.Add(RESPONSE_HEADER, string.Format("Hello {0} time is {1}", name, DateTime.Now));
            }
            return response;
        }
    }
}