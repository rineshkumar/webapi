﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;

namespace TalentManager.Web.App_Start
{
    public class ConflictExceptionHandlerAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            string message = "Problem with the reuqest";
            if (true/**context.Exception is DbUpdateConcurrencyException*/)
            {

                context.Response = new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
    }
}