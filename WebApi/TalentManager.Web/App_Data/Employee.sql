﻿CREATE TABLE [dbo].employee
(
	employee_id INT IDENTITY(1,1) PRIMARY KEY , 
    [first_name] VARCHAR(50) NULL, 
    [last_name] VARCHAR(50) NULL, 
    [department_id] INT NULL, 
    [row_version] TIMESTAMP NULL, 
    CONSTRAINT [FK_employee_Department] FOREIGN KEY (department_id) REFERENCES Department(department_id)
)