﻿CREATE TABLE [dbo].[Department]
(
	[department_id] INT IDENTITY(1,1) PRIMARY KEY , 
    [name] VARCHAR(50) NULL, 
    [row_version] TIMESTAMP NULL
)
