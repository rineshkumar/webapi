﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TalentManager.Data;
using TalentManager.Domain;
using AutoMapper;
using TalentManager.Web.Models;
using TalentManager.Web.App_Start;


namespace TalentManager.Web.Controllers
{
    public class EmployeesV2Controller : ApiController
    {
        private readonly IUnitOfWork uow = null;
        private readonly IRepository<Employee> repository = null;
        private readonly IMappingEngine mapper = null;
        public EmployeesV2Controller()
        {
            uow = new UnitOfWork();
            repository = new Repository<Employee>(uow);
            mapper = Mapper.Engine;
        }

        public EmployeesV2Controller(IUnitOfWork uow, IRepository<Employee> repository, IMappingEngine mapper)
        {
            this.uow = uow;
            this.repository = repository;
            this.mapper = mapper;
        }
        [OptimisticLock]
        public HttpResponseMessage Get(int id)
        {
            var employee = repository.Find(id);
            if (employee == null)
            {
                var response = Request.CreateResponse(HttpStatusCode.NotFound,
                "Employee not found");
                throw new HttpResponseException(response);
            }
            return Request.CreateResponse<EmployeeDto>(
            HttpStatusCode.OK,
            mapper.Map<Employee, EmployeeDto>(employee));
        }

        public HttpResponseMessage GetByDepartment(int departmentId)
        {
            var employees = repository.All.Where(e => e.DepartmentId == departmentId);
            if (employees != null && employees.Any())
            {
                return Request.CreateResponse<IEnumerable<Employee>>(
                HttpStatusCode.OK, employees);
            }
            throw new HttpResponseException(HttpStatusCode.NotFound);
        }

        public HttpResponseMessage Post(EmployeeDto employeeDto)
        {
            var employee = mapper.Map<EmployeeDto, Employee>(employeeDto);
            repository.Insert(employee);
            uow.Save();
            var response = Request.CreateResponse<Employee>(HttpStatusCode.Created, employee);
            string uri = Url.Link("DefaultApi", new { id = employee.Id });
            response.Headers.Location = new Uri(uri);
            return response;
        }
        [ConflictExceptionHandler]
        [OptimisticLock]
        public void Put(int id, EmployeeDto employeeDto)
        {
            var employee = mapper.Map<EmployeeDto, Employee>(employeeDto);
            repository.Update(employee);
            uow.Save();
        }
        public void Delete(int id)
        {
            repository.Delete(id);
            uow.Save();

        }
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}