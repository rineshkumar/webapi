﻿using HelloWebApi.App_Start;
using HelloWebApi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace HelloWebApi.Controllers
{
    //http://localhost:64226/api/Employees
    public class EmployeesController : ApiController
    {
        //private static IList<Employee> list = new List<Employee>()
        //{
        //    new Employee()
        //    {
        //        Id = 12345, FirstName = "John", LastName = "ようこそいらっしゃいました。",Department = 2,Compensation = 45678.12M,Doj = new DateTime(1990, 06, 02)
            
        //    },
        //    new Employee()
        //    {
        //        Id = 12346, FirstName = "Jane", LastName = "Public",Department = 3
        //    },
        //    new Employee()
        //    {
        //        Id = 12347, FirstName = "Joseph", LastName = "Law",Department = 2
        //    }
        //};
        //Action methods . 
        //get,post,put,delete 
        //public HttpResponseMessage Get(int id)
        //{
        //    var blackListed = "application/json";
        //    var allowedFormatters = Configuration.Formatters
        //        .Where(f => !f.SupportedMediaTypes
        //        .Any(m => m.MediaType
        //        .Equals(blackListed,
        //        StringComparison.OrdinalIgnoreCase)));
        //    var result = Configuration.Services.GetContentNegotiator().Negotiate(typeof(Employee),Request,allowedFormatters);
        //    if (result == null)
        //        throw new HttpResponseException(System.Net.HttpStatusCode.NotAcceptable);
        //    var emp = list.Where(e => e.Id == id).FirstOrDefault();
        //    if (emp == null)
        //    {
        //        throw new HttpResponseException(HttpStatusCode.NotFound);
        //    }
        //    HttpResponseMessage response = new HttpResponseMessage();
        //    response.Content = new ObjectContent<Employee>(emp, result.Formatter,result.MediaType);
        // //   return emp;
        //    return response;
        //}

        //public IEnumerable<Employee> Get([FromUri]Filter filter)
        //{
        //    return list.Where(e => e.Department == filter.Department &&
        //    e.LastName == filter.LastName);
        //}
        //public Employee Get(int id)
        //{
        //    var employee = list.FirstOrDefault(e => e.Id == id);
        //    if (employee == null)
        //    {
        //        var response = Request.CreateResponse(HttpStatusCode.NotFound,
        //        new HttpError(Resources.Messages.NotFound));
        //        throw new HttpResponseException(response);

        //    }
        //    return employee;
        //}

        //public HttpResponseMessage Get()
        //{
        //    var values = list.Select(e => new
        //    {
        //        Identifier = e.Id,
        //        Name = e.FirstName + " " + e.LastName
        //    });
        //    var response = new HttpResponseMessage(HttpStatusCode.OK)
        //    {
        //        Content = new ObjectContent(values.GetType(),
        //        values,
        //        Configuration.Formatters.JsonFormatter)
        //    };
        //    return response;
        //}
        //{"Id":12345,"FirstName":"John","LastName":"Human"}
        //<Employee xmlns="http://schemas.datacontract.org/2004/07/HelloWebApi.Models">
        //    <FirstName>John</FirstName>
        //    <Id>12345</Id>
        //    <LastName>Human</LastName>
        //</Employee>
        /*
         http://localhost:64226/api/employees/12345?firstName=John&locationId=12&guid=31c9359d-d332-4703-a896-7e9655eff171
         */
        /*
         *  http://localhost:64226/api/employees/12345?firstName=John&guid=31c9359d-d332-4703-a896-7e9655eff171
         * 
         *  Content-Type: application/json
         *  Dont use {"location":10} only use 10 in request body
         */
        //public void Post(int id, [FromUri]Employee employee)
        //{
        //    Trace.WriteLine(employee.Id);

        //}
        /*
         * http://localhost:64226/api/employees/12345?nicknames=Liz&nicknames=Beth
         * Content-Length: 0
         * [FromUri]
         */
        /*
         * Content-Type: application/x-www-form-urlencoded
         * Request bosy : firstname=John&lastname=Human+Being
         */
        /*
         * Reading Date 
         * {"Id":12345,"FirstName":"John","LastName":"Human","Doj":"1998-06-02T00:00:00"}
         */
        /*
         * {"Id":12345,"FirstName":"John","LastName":"Human","Doj":"06/02/1998"}
         * DOj interpertation will be different for acceptlanguage en-us and fr-fr 
         * due to DateTimeConverter and culturehandler
         */
        public void Post( Employee employee)
        {
            if (ModelState.IsValid)
            {
                Trace.WriteLine(employee.FirstName);
            }
            else {
                var errors = ModelState.Where(e => e.Value.Errors.Count > 0)
                    .Select(e => new { Name = e.Key, Message = e.Value.Errors.First().ErrorMessage, Exception = e.Value.Errors.First().Exception }).ToList();

            }
        }
        //public HttpResponseMessage Get(Shift shift)
        //{
        //    // Do something with shift
        //    var response = new HttpResponseMessage(HttpStatusCode.OK)
        //    {
        //        Content = new StringContent("")
        //    };
        //    return response;
        //}
        //public HttpResponseMessage Get([System.Web.Http.ModelBinding.ModelBinder]IEnumerable<string> ifmatch)
        //{
        //    var response = new HttpResponseMessage(HttpStatusCode.OK)
        //    {
        //        Content = new StringContent(ifmatch.First().ToString())
        //    };
        //    return response;
        //}
        public HttpResponseMessage Get([ModelBinder(typeof(TalentScoutModelBinderProvider))]TalentScout scout)
        {
            // Do your logic with scout model
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent("")
            };
            return response;
        }
        /**
         * http://localhost:64226/api/employees/12345?doj=6/2/1998
         * 
         * Content-Type: application/json
            X-Affiliation: Green Planet

         * {"FirstName":"John", "LastName":"Human"}
         */
        /*
         * 012345John                Human               |
         * http://localhost:64226/api/employees/12345
         * Content-Type: text/plain;charset=utf-16
         */
        public void Put(int id, Employee employee)
        {
            // Does nothing!
        }
    }
}
