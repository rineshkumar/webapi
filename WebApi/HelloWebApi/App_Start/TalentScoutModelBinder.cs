﻿using HelloWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http.ModelBinding;

namespace HelloWebApi.App_Start
{
    class TalentScoutModelBinder :IModelBinder
    {

        public bool BindModel(System.Web.Http.Controllers.HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            var scoutCriteria = (TalentScout)bindingContext.Model ?? new TalentScout();
            var result = bindingContext.ValueProvider.GetValue("dept");
            if (result != null)
                scoutCriteria.Departments = result.AttemptedValue
                .Split(',')
                .Select(d => d.Trim()).ToList();
            result = bindingContext.ValueProvider.GetValue("xctcbased");
            if (result != null)
            {
                int basedOn;
                if (Int32.TryParse(result.AttemptedValue, out basedOn))
                {
                    scoutCriteria.IsCtcBased = (basedOn > 0);
                }
            }
            result = bindingContext.ValueProvider.GetValue("doj");
            if (result != null)
            {
                DateTime doj;
                if (DateTime.TryParse(result.AttemptedValue, out doj))
                {
                    scoutCriteria.Doj = doj;
                }
            }
            bindingContext.Model = scoutCriteria;
            return true;
        }
    }
}
