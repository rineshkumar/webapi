﻿using HelloWebApi.App_Start;
using HelloWebApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;

namespace HelloWebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //config.Routes.MapHttpRoute(
            //    name: "RpcApi",
            //    routeTemplate: "api/{controller}/{action}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //    );
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            foreach (var formatter in config.Formatters)
            {
                Trace.WriteLine(formatter.GetType().Name);
                Trace.WriteLine("\tCanReadType: " + formatter.CanReadType(typeof(Employee)));
                Trace.WriteLine("\tCanWriteType: " + formatter.CanWriteType(typeof(Employee)));
                Trace.WriteLine("\tBase: " + formatter.GetType().BaseType.Name);
                Trace.WriteLine("\tMedia Types: " + String.Join(", ", formatter.
                SupportedMediaTypes));
            }
            //            config.Formatters.RemoveAt(0);
            config.Formatters.JsonFormatter.MediaTypeMappings.Add(new QueryStringMapping("frmt", "json", new MediaTypeHeaderValue("application/json")));
            config.Formatters.XmlFormatter.MediaTypeMappings.Add(new QueryStringMapping("frmt", "xml", new MediaTypeHeaderValue("application/xml")));

            config.Formatters.JsonFormatter.MediaTypeMappings.Add(new RequestHeaderMapping("x-media","json",StringComparison.OrdinalIgnoreCase,false,new MediaTypeHeaderValue("application/json")));
            config.Formatters.JsonFormatter.MediaTypeMappings.Add(new RequestHeaderMapping("x-media", "xml", StringComparison.OrdinalIgnoreCase, false, new MediaTypeHeaderValue("application/xml")));
            

            config.Formatters.XmlFormatter.MediaTypeMappings.Add(new UserAgentBasedMapping());
            var fwtMediaFormatter = new FixedWidthTextMediaFormatter();
            fwtMediaFormatter.MediaTypeMappings.Add(new QueryStringMapping("frmt", "text",new MediaTypeHeaderValue("text/plain")));
            config.Formatters.Add(fwtMediaFormatter);

            config.Formatters.Add(new JsonpMediaTypeFormatter());

            //using xml formatter 
            config.Formatters.XmlFormatter.UseXmlSerializer = true;

            config.Formatters.JsonFormatter.SerializerSettings.Formatting = Formatting.Indented;
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();


            foreach (var encoding in config.Formatters.JsonFormatter.SupportedEncodings)
            {
                System.Diagnostics.Trace.WriteLine(encoding.WebName);
            }
            foreach (var validatorProvider in config.Services.GetModelValidatorProviders().ToList())
            {
                System.Diagnostics.Trace.WriteLine(validatorProvider.ToString());
                
            }
            //Adding converter for various alanguages 
            config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new NumberConverter());
            config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new DateTimeConverter());
            //using DBCS (double byte charaacter set ) and encoding shift-jis 
            //config.Formatters.JsonFormatter.SupportedEncodings.Clear();
            config.Formatters.JsonFormatter.SupportedEncodings.Add(Encoding.GetEncoding(932));//932 is for shift-jis
            config.Services.Add(typeof(System.Web.Http.ValueProviders.ValueProviderFactory),new HeaderValueProviderFactory());

            //var rules = config.ParameterBindingRules;
            //rules.Insert(0, p =>
            //{

            //    if (p.ParameterType == typeof(Employee))
            //    {
            //        return new AllRequestParameterBinding(p);
            //    }
            //    return null;
            //});
            config.Filters.Add(new ValidationErrorHandlerFilterAttribute());
            //adding handler for encoding 
            config.MessageHandlers.Add(new EncodingHandler());
            config.MessageHandlers.Add(new CultureHandler());
            // Other handlers go here

            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();

            // To disable tracing in your application, please comment out or remove the following line of code
            // For more information, refer to: http://www.asp.net/web-api
            config.EnableSystemDiagnosticsTracing();
        }
    }
}
