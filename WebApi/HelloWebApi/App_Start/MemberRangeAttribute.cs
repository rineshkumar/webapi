﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HelloWebApi.App_Start
{
    public class MemberRangeAttribute : RangeAttribute
    {
        public MemberRangeAttribute(int minimum, int maximum) : base(minimum, maximum) { }
        public override bool IsValid(object value)
        {
            if (value is ICollection<int>)
            {
                var items = (ICollection<int>)value;
                return items.Cast<int>().All(i => IsValid(i));
            }
            else
                return base.IsValid(value);
        }
    }
}