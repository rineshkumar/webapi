﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.ValueProviders;

namespace HelloWebApi.App_Start
{
    public class HeaderValueProviderFactory : ValueProviderFactory
    {
        public override IValueProvider GetValueProvider(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var request = actionContext.ControllerContext.Request;
            return new HeaderValueProvider(request.Headers);
        }
    }
}