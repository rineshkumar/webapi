﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web;

namespace HelloWebApi.App_Start
{
    public class UserAgentBasedMapping : MediaTypeMapping
    {
        public UserAgentBasedMapping() : base(new MediaTypeHeaderValue("application/xml")) { }
        public override double TryMatchMediaType(System.Net.Http.HttpRequestMessage request)
        {
            if (request.Headers.UserAgent.ToString().Equals("Fiddler1"))
            {
                return 1.0;
            }
            else {
                return 0.0;
            }
        }
    }
}