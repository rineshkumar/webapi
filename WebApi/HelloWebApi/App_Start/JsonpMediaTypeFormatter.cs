﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HelloWebApi.App_Start
{
    public class JsonpMediaTypeFormatter : JsonMediaTypeFormatter
    {
        private const string JAVASCRIPT_MIME = "application/javascript";
        private string queryStringParameterName = "callback";
        private string Callback { get; set; }
        private bool IsJsonp { get; set; }
        public JsonpMediaTypeFormatter()
        {
            SupportedMediaTypes.Clear();
            MediaTypeMappings.Clear();

            SupportedMediaTypes.Add(new MediaTypeHeaderValue(JAVASCRIPT_MIME));
            MediaTypeMappings.Add(new QueryStringMapping("frmt", "jsonp", JAVASCRIPT_MIME));


        }
        public override bool CanReadType(Type type)
        {
            return false;
        }
        public override MediaTypeFormatter GetPerRequestFormatterInstance(Type type, System.Net.Http.HttpRequestMessage request, MediaTypeHeaderValue mediaType)
        {
            bool isGet = request != null && request.Method == HttpMethod.Get;
            string callback = string.Empty;
            if (request.RequestUri != null)
            {
                callback = HttpUtility.ParseQueryString(request.RequestUri.Query)[queryStringParameterName];
            }
            bool isJsonp = isGet && !String.IsNullOrEmpty(callback);
            return new JsonpMediaTypeFormatter() { Callback = callback, IsJsonp = isJsonp };
        }
        public override void SetDefaultContentHeaders(Type type, HttpContentHeaders headers, MediaTypeHeaderValue mediaType)
        {
            base.SetDefaultContentHeaders(type, headers, mediaType);
            if (!this.IsJsonp)
            {
                // Fallback to JSON content type
                headers.ContentType = DefaultMediaType;
                // If the encodings supported by us include the charset of the
                // authoritative media type passed to us, we can take that as the charset
                // for encoding the output stream. If not, pick the first one from
                // the encodings we support.
                if (this.SupportedEncodings.Any(e => e.WebName.Equals(mediaType.CharSet,
                StringComparison.OrdinalIgnoreCase)))
                    headers.ContentType.CharSet = mediaType.CharSet;
                else
                    headers.ContentType.CharSet = this.SupportedEncodings.First().WebName;
            }
        }
        public override async Task WriteToStreamAsync(Type type, object value,
 Stream stream,
 HttpContent content,
 TransportContext transportContext)
        {
            using (stream)
            {
                if (this.IsJsonp) // JSONP
                {
                    Encoding encoding = Encoding.GetEncoding
                    (content.Headers.ContentType.CharSet);
                    using (var writer = new StreamWriter(stream, encoding))
                    {
                        writer.Write(this.Callback + "(");
                        await writer.FlushAsync();
                        await base.WriteToStreamAsync(type, value, stream, content,
                        transportContext);
                        writer.Write(")");
                        await writer.FlushAsync();
                    }
                }
                else // fallback to JSON
                {
                    await base.WriteToStreamAsync(type, value, stream, content,
                    transportContext);
                    return;
                }
            }
        }
    }
}