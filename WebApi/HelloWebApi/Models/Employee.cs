﻿using HelloWebApi.App_Start;
using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HelloWebApi.Models
{
    public class Employee : IValidatableObject
    {
        
        public int Id { get; set; }
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        public int Department { get; set; }
        public decimal Compensation { get; set; }
        public DateTime Doj { get; set; }
        public string Xaffiliation { get; set; }
        public decimal AnnualIncome { get; set; }
        
        public decimal Contribution401K { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (this.Id < 10000 || this.Id > 99999)
                yield return new ValidationResult("ID must be in the range 10000 - 99999");
            if (String.IsNullOrEmpty(this.LastName))
                yield return new ValidationResult("Last Name is mandatory");
        }
    }
}