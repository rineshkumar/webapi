﻿using Robusta.TalentManager.WebApi.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace Robusta.TalentManager.WebApi.Client.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // GetData();
            //PostData();
          //  GetDataWithMeadiaTypeHeader();
            GetDataUsingAuthenticationHeader();

        }

        private static void GetDataUsingAuthenticationHeader()
        {
            HttpClient client = HttpClientFactory.Create(new CredentialsHandler());
            client.BaseAddress = new Uri(GetBaseUrl());
            HttpResponseMessage response = client.GetAsync("employees/1").Result;
            Console.WriteLine("{0} - {1}", (int)response.StatusCode, response.ReasonPhrase);
            if (response.IsSuccessStatusCode)
            {
                var employee = response.Content.ReadAsAsync<EmployeeDto>().Result;
                Console.WriteLine("{0}\t{1}\t{2}",
                employee.Id,
                employee.FirstName,
                employee.LastName,
                employee.DepartmentId);
            }
            Console.ReadLine();
        }

        private static void GetDataWithMeadiaTypeHeader()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(GetBaseUrl());
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json", 0.8));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml", 0.9));
            HttpResponseMessage response = client.GetAsync("employees/1").Result;
            Console.WriteLine("{0} - {1}", (int)response.StatusCode, response.ReasonPhrase);
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            }
            Console.ReadLine();
        }

        private static string GetBaseUrl()
        {
            return "http://localhost:64430/api/";
        }

        private static void PostData()
        {
            HttpClient client = new HttpClient();
            EmployeeDto newEmployee = new EmployeeDto()
            {
                FirstName = "Julian",
                LastName = "Heineken",
                DepartmentId = 2
            };
            HttpResponseMessage response = client.PostAsJsonAsync<EmployeeDto>
            (GetBaseUrl()+"employees", newEmployee)
            .Result;
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                if (response.Headers != null)
                    Console.WriteLine(response.Headers.Location);
            }
            Console.WriteLine("{0} - {1}", (int)response.StatusCode, response.ReasonPhrase);
            Console.ReadLine();
        }

        private static void GetData()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(GetBaseUrl());
            HttpResponseMessage response = client.GetAsync("employees/1").Result;
            Console.WriteLine("{0} - {1}", (int)response.StatusCode, response.ReasonPhrase);
            if (response.IsSuccessStatusCode)
            {
                var employee = response.Content.ReadAsAsync<EmployeeDto>().Result;
                Console.WriteLine("{0}\t{1}\t{2}",
                employee.Id,
                employee.FirstName,
                employee.LastName,
                employee.DepartmentId);
            }
            Console.ReadLine();
        }

    }
}
