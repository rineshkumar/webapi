﻿using Robusta.TalentManager.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Robusta.TalentManager.Data
{
    public interface IContext : IDisposable
    {
        int SaveChanges();
    }
    public interface IRepository<T> : IDisposable where T : class, IIdentifiable
    {
        IQueryable<T> All { get; }
        IQueryable<T> AllEager(params Expression<Func<T, object>>[] includes);
        T Find(int id);
        void Insert(T entity);
        void Update(T entity);
        void Delete(int id);
    }
    public interface IUnitOfWork : IDisposable
    {
        int Save();
        IContext Context { get; }
    }
}
