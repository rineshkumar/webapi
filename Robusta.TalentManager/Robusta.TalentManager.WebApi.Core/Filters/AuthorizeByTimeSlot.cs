﻿using System;
using System.Web.Http;
using System.Web.Http.Controllers;
public class AuthorizeByTimeSlot : AuthorizeAttribute
{
    public int SlotStartHour { get; set; }
    public int SlotEndHour { get; set; }
    protected override bool IsAuthorized(HttpActionContext context)
    {
        if (DateTime.Now.Hour >= this.SlotStartHour &&
        DateTime.Now.Hour <= this.SlotEndHour &&
        base.IsAuthorized(context))
            return true;
        return false;
    }
}